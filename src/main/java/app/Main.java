package app;


import app.tree.ListTree;
import app.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        ListTree<Integer, String> tree = new ListTree<>();

        tree.put(13, "data13");
        tree.put(8, "data8");
        tree.put(17, "data17");
        tree.put(1, "data1");
        tree.put(11, "data11");
        tree.put(15, "data15");
        tree.put(25, "data25");
        tree.put(6, "data6");
        tree.put(22, "data22");
        tree.put(27, "data27");
        tree.print();
        tree.remove(15);
        System.out.println("____________________________________________");
        tree.print();
    }
}
