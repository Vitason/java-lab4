package app.tree;

import java.util.*;

public class TreeNode<T> implements Iterable<TreeNode<T>> {


    protected T data;
    protected TreeNode<T> parent;
    protected List<TreeNode<T>> children;

    public TreeNode(T data) {
        this.data = data;
        this.parent = null;
        this.children = new ArrayList<>();
    }

    public void addChild(TreeNode<T> child) {
        if (child == null) return;
        if (child.parent != null) {
            child.parent.removeChild(child);
        }
        child.parent = this;
        children.add(child);
    }

    public void addChildren(List<TreeNode<T>> children) {
        if (children == null) return;
        children.forEach(child -> {
            if (child.parent != null) {
                child.parent.removeChild(child);
            }
            child.parent = this;
        });
        this.children.addAll(children);
    }

    public void setParent(TreeNode<T> newParent) {
//        if (newParent == null) return;
//        if (parent != null && parent != newParent) {
//            parent.removeChild(this);
//        }
        parent = newParent;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public TreeNode<T> getChild(int index) {
        if (index < 0 || index >= children.size()) return null;
        return children.get(index);
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public TreeNode<T> removeChild(int index) {
        if (index < 0 || index >= children.size()) return null;
        TreeNode<T> removedChild = children.remove(index);
        removedChild.parent = null;
        return removedChild;
    }

    public boolean removeChild(TreeNode<T> child) {
        child.parent = null;
        return children.remove(child);
    }

    public TreeNode<T> replaceChild(int index, TreeNode<T> newChild) {
        if (index < 0 || index >= children.size() || newChild == null) return null;
        TreeNode<T> childForReplace = children.get(index);
        if (childForReplace != null) childForReplace.parent = null;
        children.set(index, newChild);
        if (newChild.parent != null) {
            newChild.parent.removeChild(newChild);
        }
        newChild.parent = this;
        return childForReplace;
    }

    //Variant №15 PRE-ORDER
    public static<T> List<T> DepthFirstTraversal(TreeNode<T> root) {

        List<T> result = new ArrayList<>();
        //
//        Stack<TreeNode<T>> stack = new Stack<>();
//        stack.push(root);
//        while(!stack.isEmpty()) {
//            TreeNode<T> current = stack.pop();
//            result.add(current.getData());
//            for (int i = current.children.size() - 1; i >= 0; --i) {
//                stack.push(current.children.get(i));
//            }
//        }
//        return result;
        //
        depthFirstIteration(root, result);
        return result;
    }

    public static<T> void depthFirstIteration(TreeNode<T> node, List<T> list) {
        if (node == null) return;
        list.add(node.getData());
        node.children.forEach(child -> {
            depthFirstIteration(child, list);
        });
    }

    public static<T> List<T> BreadthFirstTraversal(TreeNode<T> root) {
        if (root == null) return null;
        List<T> result = new ArrayList<>();
        Queue<TreeNode<T>> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()){
            TreeNode<T> node = queue.remove();
            result.add(node.getData());
            queue.addAll(node.children);
        }
        return result;
    }

    public T getData() {
        return data;
    }

    public void setData(T newData) {
        data = newData;
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        List<TreeNode<T>> result = new ArrayList<>();
        result.add(this);
        for(int i = 0; i < result.size(); ++i){
            result.addAll(result.get(i).getChildren());
        }
        return result.iterator();
    }
}
