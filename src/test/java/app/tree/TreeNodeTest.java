package app.tree;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TreeNodeTest {

    @Test
    void addChild() {
        TreeNode<String> strNode = new TreeNode<>("");

        strNode.addChild(new TreeNode<>("data"));
        List<TreeNode<String>> children = strNode.getChildren();
        assertEquals(1, children.size());
        assertEquals("data", children.get(0).getData());
    }

    @Test
    void addChildren() {
        TreeNode<String> strNode = new TreeNode<>("");
        List<TreeNode<String>> children = new ArrayList<>();
        children.add(new TreeNode<>("data1"));
        children.add(new TreeNode<>("data2"));
        children.add(new TreeNode<>("data3"));
        strNode.addChildren(children);

        List<TreeNode<String>> expected_children = strNode.getChildren();
        assertEquals(3, expected_children.size());
        assertEquals("data1", expected_children.get(0).getData());
        assertEquals("data2", expected_children.get(1).getData());
        assertEquals("data3", expected_children.get(2).getData());
    }

    @Test
    void getParent() {
        TreeNode<String> strNode1 = new TreeNode<>("data");
        TreeNode<String> strNode2 = new TreeNode<>("");
        strNode1.addChild(strNode2);

        assertNotEquals(null, strNode2.getParent());
        assertEquals("data", strNode2.getParent().getData());
    }

    @Test
    void getChild() {
        TreeNode<String> strNode1 = new TreeNode<>("data");
        TreeNode<String> strNode2 = new TreeNode<>("");
        strNode1.addChild(strNode2);

        assertEquals(strNode1.getChild(0), strNode2);
    }

    @Test
    void getChildren() {
        TreeNode<String> strNode = new TreeNode<>("");
        List<TreeNode<String>> children = new ArrayList<>();
        children.add(new TreeNode<>("data1"));
        children.add(new TreeNode<>("data2"));
        children.add(new TreeNode<>("data3"));
        strNode.addChildren(children);

        assertEquals(3, strNode.getChildren().size());
    }

    @Test
    void removeChild() {
        TreeNode<String> strNode = new TreeNode<>("");
        List<TreeNode<String>> children = new ArrayList<>();
        children.add(new TreeNode<>("data1"));
        children.add(new TreeNode<>("data2"));
        children.add(new TreeNode<>("data3"));
        strNode.addChildren(children);

        assertEquals("data1", strNode.getChild(0).getData());
        strNode.removeChild(0);
        assertEquals("data2", strNode.getChild(0).getData());
    }

    @Test
    void replaceChild() {
        TreeNode<String> strNode = new TreeNode<>("");
        List<TreeNode<String>> children = new ArrayList<>();
        children.add(new TreeNode<>("data1"));
        children.add(new TreeNode<>("data2"));
        strNode.addChildren(children);

        TreeNode<String> child = new TreeNode<>("data3");

        assertEquals("data1", strNode.getChild(0).getData());
        TreeNode<String> replacedChild = strNode.replaceChild(0, child);
        assertEquals("data3", strNode.getChild(0).getData());
        assertEquals("data1", replacedChild.getData());
    }
}